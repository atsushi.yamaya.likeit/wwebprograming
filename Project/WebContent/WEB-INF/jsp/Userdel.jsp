<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>
<head>
<a href= "LogoutServlet" class="navbar-link logout-link">ログアウト</a>
</head>
	<body>
		<h1 align="center">ユーザ削除確認</h1>
        <p align="center">ログインID: ${user.loginId }<br>を本当に削除してよろしいでしょうか。</p>
        
        <form action="UserListServlet" method="get">
        <button type="submit" value="キャンセル">キャンセル</button>
        </form>
        
        <form action="UserDeleteServlet2?id=${user.id }" method="post">
        <button type="submit" value="OK" href="UserDeleteServlet2?" style="width:80px;height:25px;" >OK</button>
        </form>
        
         </body>
</html>