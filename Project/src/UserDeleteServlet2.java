

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserDeleteServlet2
 */
@WebServlet("/UserDeleteServlet2")
public class UserDeleteServlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet2() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		 request.setCharacterEncoding("UTF-8");
		 
		 String id = request.getParameter("id");
			
			if(id == null) {
				response.sendRedirect("Login");
				return;
			}

		 
		 UserDao userDao = new UserDao();
		model.User user = UserDao.Detail(id);
			
		request.setAttribute("user",user);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/Userdel.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		String id = request.getParameter("id");
		System.out.println(id);
		
		UserDao userDao = new UserDao();
		
		try {
			model.User user = UserDao.delete(id);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		
		response.sendRedirect("UserListServlet");
	}
}

