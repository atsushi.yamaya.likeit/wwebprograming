<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head><a href= "LogoutServlet" class="navbar-link logout-link">ログアウト</a>
</head>
	<body>
		<c:if test="${errMsg != null }">
			<font color="red">
				<div class="alert alert-danger" role="alert" align="center">
				${errMsg }
				</div>
			</font>
		</c:if>

		<h1 align="center">ユーザ情報更新</h1>


		<form class="Update" action="UserUpdateServlet" method="post">

        <input type="hidden" name="id" value="${user.id }">

        <p align="center" name="login_id" id="login_id">ログインID ${user.loginId }</p>

        <div align="center"  id="password">パスワード
        	<input type="password" name="password">
        </div>

        <div align="center" id="check_password">パスワード（確認）
        	<input type="password" name="check_password">
        </div>

        <div align="center" id="name">ユーザ名
        	<input type="text" value="${user.name }" name="name">
        </div>

        <div align="center" id="birth_date" >生年月日
        	<input type="date" name="birth_date" value="${user.birthDate }">
        </div>

        <div align="center">
        <button type="submit" value="更新" style="width:100px;height:25px" >更新</button>
        </form>
        </div>

         <button onclick="history.back()">戻る</button>
	</body>
</html>