
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="viewpoint" content="width=divice-width,initiai-scale-1">
<title>UserList</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/original/common.css" rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<header>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<ul class="nav navbar-nav navbar-right">
				<li class="navbar-text">${userInfo.name }さん</li>
				<li class="dropdown"><a href="LogoutServlet"
					class="navbar-link logout-link">ログアウト</a></li>
			</ul>
		</div>
	</nav>
</header>

<body>
	<div class="container">
		<h1 align="center">ユーザ一覧</h1>

		<div align="right">
			<a href="UserSetServlet?id=${user.id }">新規登録</a>
		</div>

		<div class="panel-body">
			<div class="panel panel-default"></div>
			<div class="panel-body">
				<form method="post" action="UserListServlet" class="form-horizontal">
					<div class="form-group">
						<label for="code" class="contol-label col-sm-2">ログインID</label>
						<div class="col-sm-6">
							<input type="text" name="login-id" id="login-id"
								class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label for="name" class="control-label col-sm-2">ユーザ名</label>
						<div class="col-sm-6">
							<input type="text" name="user-name" id="user-name"
								class="form-control" />
						</div>
					</div>

					<div class="form-group">
						<label for="continent" class="control-label col-sm-2">生年月日</label>
						<div class="row">
							<div class="col-sm-2">
								<input type="date" name="date-start" id="date-start"
									class="form-control" size="30" />
							</div>
							<div class="col-xs-1 text-center">~</div>
							<div class="col-sm-2">
								<input type="date" name="date-end" id="date-end"
									class="form-control" />
							</div>
						</div>
					</div>

					<div align="right">
						<button type="submit" value="検索"
							class="btn btn-primary form-submit">検索</button>
					</div>
				</form>
			</div>
		</div>

		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<c:forEach var="user" items="${userList }">
						<tr>
							<td>${user.loginId }</td>
							<td>${user.name }</td>
							<td>${user.birthDate }</td>

							<c:if test="${userInfo.loginId == 'admin'}">
								<td><a class="btn btn-primary"
									href="UserDetailServlet?id=${user.id }">詳細</a> <a
									class="btn btn-success" href="UserUpdateServlet?id=${user.id }">更新</a>
									<a class="btn btn-danger"
									href="UserDeleteServlet2?id=${user.id }">削除</a></td>
							</c:if>

							<c:if test="${userInfo.loginId != 'admin'}">
								<td><a class="btn btn-primary"
									href="UserDetailServlet?id=${user.id }">詳細</a> 
									
									<c:if
										test="${userInfo.loginId  == user.loginId}">
										<a class="btn btn-success"
											href="UserUpdateServlet?id=${user.id }">更新 </a>
									</c:if></td>

							</c:if>

						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>

</body>
</html>
