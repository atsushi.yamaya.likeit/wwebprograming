<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>UserinfoS</title>

<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
</head>
<body>
	<h1 align="center">ユーザ情報詳細参照</h1>

	<div class="col-2 mx-auto">
		<div class="row">
			ログインID
			<div>${user.loginId }</div>
		</div>

		<div class="row">
			ユーザ名
			<div>${user.name }</div>
		</div>

		<div class="row">
			生年月日
			<div>${user.birthDate }</div>
		</div>

		<div class="row">
			登録日時
			<div>${user.createDate }</div>
		</div>

		<div class="row">
			更新日時
			<div>${user.updateDate }</div>
		</div>

		<button onclick="history.back()">戻る</button>
	</div>
</body>
</html>