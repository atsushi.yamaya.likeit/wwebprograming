
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserSetServlet
 */
@WebServlet("/UserSetServlet")
public class UserSetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSetServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String id = request.getParameter("id");
		
		if(id == null) {
			response.sendRedirect("Login");
			return;
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("login_id");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birth_date");
		String password = request.getParameter("password");
		String Check_password = request.getParameter("check_password");

		String str = "";
		if (str.equals("loginId") || str.equals("name") || str.equals("birthDate") || str.equals("password")) {
			request.setAttribute("errMsg", "入力された値は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User.jsp");
			dispatcher.forward(request, response);
			return;

			
		} else if (!(password.equals(Check_password))) {
			request.setAttribute("errMsg", "入力された値は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User.jsp");
			dispatcher.forward(request, response);
			return;

		}
		
		UserDao userDao = new UserDao();
		User user = userDao.Check(loginId);
		
		if(user != null) {
			request.setAttribute("errMsg", "入力された値は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User.jsp");
			dispatcher.forward(request, response);
			return;

		}
		
		UserDao.Set(loginId, name, birthDate, password);

		response.sendRedirect("UserListServlet");

	}
}