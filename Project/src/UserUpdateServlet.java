
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id = request.getParameter("id");
		
		if(id == null) {
			response.sendRedirect("Login");
			return;
		}

		UserDao userDao = new UserDao();
		User user = UserDao.Detail(id);

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserinfoK.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		request.setCharacterEncoding("UTF-8");
		
		

		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String check_password = request.getParameter("check_password");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birth_date");
		

		if (id.equals("") || id == null || birthDate.equals("") || birthDate == null || name.equals("") || name == null) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");
			
			id = request.getParameter("id");
			UserDao userDao = new UserDao();
			User user = UserDao.Detail(id);
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserinfoK.jsp");
			dispatcher.forward(request, response);
			return;
			
		} else if (!(password.equals(check_password))) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			id = request.getParameter("id");
			UserDao userDao = new UserDao();
			User user = UserDao.Detail(id);
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserinfoK.jsp");
			dispatcher.forward(request, response);
			return;
			
		}else if ( !(password.equals("")) && check_password.equals(""))   {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			id = request.getParameter("id");
			UserDao userDao = new UserDao();
			User user = UserDao.Detail(id);
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserinfoK.jsp");
			dispatcher.forward(request, response);
			return;
			
		}else if( (password.equals("") && !(check_password.equals("")))) {
			
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			id = request.getParameter("id");
			UserDao userDao = new UserDao();
			User user = UserDao.Detail(id);
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/UserinfoK.jsp");
			dispatcher.forward(request, response);
			
			return;
		}

			UserDao userdao = new UserDao();
			UserDao.Update(id, password, name, birthDate);
			response.sendRedirect("UserListServlet");
		}
	}

