<%@ page language="java" contentType="text/html; charset=UTF-8"   pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>
<head>
</head>
    <title> login</title>
	<body class="log">
        <h1 align="center">ログイン画面</h1>
        <p></p>
        
       <c:if test="${errMsg != null}">
        <div class="alert alert-danger" role="alert">
        ${errMsg}
        </div>
        </c:if>
        
        <div align="center">
        <form class= "form-signin" action="Login" method="post">
        <input type="text" name="login_id" id="login_id" class="form-control" >
          <input type="password" name="password" id="password" class="form-control" >
          </div>
        <div align="center">
        <button class="btn btn-lg btn-primry btn-block btn-signin" type="submit">ログイン</button>
        </div>
        </form>
        
	</body>
</html>