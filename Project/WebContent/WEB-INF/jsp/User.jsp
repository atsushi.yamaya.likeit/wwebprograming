<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<title>User</title>
	 <a href= "LogoutServlet" class="navbar-link logout-link">ログアウト</a>
</head>
<header>
<ul> 
<li class="navbar-text">${userInfo.name}さん</li>
</ul>
</header>
	<body>
			<h1 align="center">ユーザー新規登録</h1>

       <c:if test="${errMsg != null}">
       <font color="red">
        <div class="alert alert-danger" role="alert" align="center">
        ${errMsg}
        </div>
          </font>
        </c:if>
        
     <form method="post">
     <div align="center">
        <div>
			<label for="login_id">ログインID</label>
			<input type="text" id= "login_id" name="login_id" >
		</div>

        <div>
        	<label for="password">パスワード</label>
        	<input type="password" id="password" name="password" >
        </div>

        <div>
        	<label for="password">パスワード（確認）</label>
        	<input type="password" id="check_passowrd" name="check_password" >
        </div>
        
        <div>
        	<label for="name">ユーザ名</label>
        	<input type="text" id="name" name="name" >
        </div>

        <div>
        	<label for="birth_date">生年月日</label>
        	<input type="Date" id="birth_date" name="birth_date" >
        </div>

       <p></p>
        <button type="submit" value="登録" style=width:120px;height:30px href="UserSetServlet">登録</button>
        </div>
        </form>

        <button onclick="history.back()">戻る</button>
	</body>
</html>