package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public static String Hash(String password) throws NoSuchAlgorithmException {
		String source = "password";
		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";

		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);

		System.out.println(result);

		return result;
	}

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, Hash(password));
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginId1 = rs.getString("login_id");
			String nameDate = rs.getString("name");

			return new User(loginId1, nameDate);

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user where login_id != 'admin'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public static void Set(String loginId, String name, String birthDate, String password) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();
			String sql = "INSERT INTO user(login_id,name,birth_date, password,create_date,update_date) VALUES (?,?,?,?,now(),now())";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, loginId);
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, Hash(password));

			int rs = pStmt.executeUpdate();

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static User Detail(String id) {
		Connection conn = null;
		try {
			conn = DBmanager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int Id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String nameDate = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String CreateDate = rs.getString("create_date");
			String UpdateDate = rs.getString("update_date");

			User user = new User(Id, loginId, nameDate, birthDate, password, CreateDate, UpdateDate);
			return user;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static void Update(String id, String password, String name, String birthDate) {
		Connection conn = null;
		try {

			conn = DBmanager.getConnection();

			String sql = "UPDATE user SET password=?,name=?,birth_date=? WHERE id=?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, Hash(password));
			pStmt.setString(2, name);
			pStmt.setString(3, birthDate);
			pStmt.setString(4, id);

			pStmt.executeUpdate();

		} catch (SQLException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static User delete(String id) throws SQLException {

		Connection conn = null;

		try {
			conn = DBmanager.getConnection();

			String sql = "DELETE FROM user WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);

			int rs = pStmt.executeUpdate();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public List<User> Reserch(String loginIdP, String UserName, String DateStart, String DateEnd) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBmanager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id != 'admin'";

			if (!loginIdP.equals("")) {
				sql += " AND login_id = '" + loginIdP + "'";
			}
			if (!UserName.equals("")) {
				sql += " AND name LIKE '" + "%" + UserName + "%" + "'";
			}
			if (!DateStart.equals("")) {
				sql += " AND birth_date >= '" + DateStart + "'";
			}
			if (!DateEnd.equals("")) {
				sql += " AND birth_date < '" + DateEnd + "'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User Check(String loginId) {
		
		Connection conn = null;
		
		try {
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user where login_id =? ";
			
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();
			
			if(!rs.next()) {
				return null;
			}
			
			String loginIdC = rs.getString("login_id");
			User user = new User(loginIdC);
				
			return new User(loginIdC);
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

}

/*
	public List<User> Reserch (String UserName) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();
		try {
			conn = DBmanager.getConnection();

			String sql = "SELECT * FROM user WHERE name LIKE ? " ;

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1,"%" +UserName +"%");
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				User user = new User(id, loginId, name, birthDate, updateDate, updateDate, updateDate);
				userList.add(user);
			}
		}
		catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
		return userList;
	}
	*/
}
